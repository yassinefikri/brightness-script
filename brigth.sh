#!/bin/bash

if [ -z "$1" ]
then
    	echo "No argument supplied"
else
	if [[ "$1" =~ ^[+-]?[0-9]+\.?[0-9]*$ ]] && [[ ! "$1" =~ ^[+-]?[0]+\.?[0]*$ ]]
	then
		display=`xrandr | grep " connected" | cut -f1 -d " "`
		xrandr --output "$display" --brightness "$1"
	else
		echo "Value is invalid"
	fi
fi
